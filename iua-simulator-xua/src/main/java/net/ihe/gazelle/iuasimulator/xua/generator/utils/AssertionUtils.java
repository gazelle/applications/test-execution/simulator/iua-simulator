package net.ihe.gazelle.iuasimulator.xua.generator.utils;

import net.ihe.gazelle.iuasimulator.xua.generator.keystoreutils.DefaultKeyStoreManager;
import net.ihe.gazelle.iuasimulator.xua.generator.keystoreutils.KeyStoreManager;
import net.ihe.gazelle.iuasimulator.xua.generator.model.AssertionAttributes;
import net.ihe.gazelle.iuasimulator.xua.generator.model.KeystoreParams;

public class AssertionUtils {

    private AssertionUtils() { }

    public static String getStringAssertionWithPath(AssertionAttributes assertionAttributes, KeystoreParams keystoreParams) throws Exception {
        KeyStoreManager keystoreObject = new DefaultKeyStoreManager(keystoreParams);
        keystoreObject.generateStores();
        return EhealthsuisseHelperService.getStringAssertion(keystoreObject, keystoreParams, assertionAttributes);
    }
}
