package net.ihe.gazelle.iuasimulator.interlay.resource.validator;

import net.ihe.gazelle.iuasimulator.application.param.IuaAuthQueryValidator;
import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;
import net.ihe.gazelle.iuasimulator.domain.model.ChAuthParamBuilder;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.ChAuthParamValidationImpl;
import net.ihe.gazelle.iuasimulator.interlay.utils.RegexUtilsImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 16/04/2023
 */
class ChCodeQueryParamValidatorImplTest {

    private final RegexUtils regexUtils = new RegexUtilsImpl();
    private final ChAuthParamValidationImpl authParam = new ChAuthParamValidationImpl();
    private final IuaAuthQueryValidator<ChAuthParamBuilder> validator = new ChAuthQueryParamValidatorImpl(authParam, regexUtils);
    private String clientName;
    private String url;

    @BeforeEach
    void setup(){
        clientName = "clientName";
        url = "http://localhost:8090/iua-simulator/rest/ch/callback";
    }

    @Test
    void validateCodeAuthorization_ko_responseType_null() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder().build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));

    }
    @Test
    void validateCodeAuthorization_ko_responseType_format() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("bad").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_responseType_blank() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_codeChallenge_format() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_codeChallenge_blank() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_state_blank() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("")
                .withScope("launch")
                .withRedirectUri("http://localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_clientId_blank() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_codeChallengeMethod_check() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("SH256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_codeChallengeMethod_blank() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_redirectUri_format() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_redirectUri_blank() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_audience_format() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("")
                .withClientId("clientName")
                .withAudience("pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_audience_blank() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_scope_blank() {
        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("")
                .withRedirectUri("localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }
}