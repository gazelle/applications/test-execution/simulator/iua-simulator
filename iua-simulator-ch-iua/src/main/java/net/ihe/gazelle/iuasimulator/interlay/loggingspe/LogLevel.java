package net.ihe.gazelle.iuasimulator.interlay.loggingspe;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 06/02/2023
 */
public enum LogLevel {
    // As defined at java.util.logging.Level
    SEVERE, WARNING, DEBUG, INFO, CONFIG, FINE, FINER, FINEST;

    @Override
    public String toString() {
        return super.toString();
    }
}
