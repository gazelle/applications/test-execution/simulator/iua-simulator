package net.ihe.gazelle.iuasimulator.interlay.resource.validator.param;

import jakarta.inject.Inject;
import net.ihe.gazelle.iuasimulator.application.validation.IuaScopeParamValidation;
import net.ihe.gazelle.iuasimulator.domain.validation.IuaScopeParamValidationImpl;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 13/04/2023
 */
public class ChScopeParamValidationImpl extends IuaScopeParamValidationImpl implements IuaScopeParamValidation {

    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.purposeOfUse")
    private String purposeOfUseRequired;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.subjectRole")
    private String subjectRoleRequired;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.principal")
    private String principalRequired;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.principalId")
    private String principalIdRequired;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.personId")
    private String personIdRequired;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.group")
    private String groupRequired;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.groupId")
    private String groupIdRequired;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.accessToken")
    private String accessTokenFormatRequired;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.subjectRoleFormat")
    private String subjectRoleFormat;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.principalIdFormat")
    private String principalIdFormat;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.groupIdFormat")
    private String groupIdFormat;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.accessTokenFormat")
    private String accessTokenFormat;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.personIdFormat")
    private String personIdFormat;
    @Inject
    @ConfigProperty(name = "ch.code.scope.validator.exception.purposeOfUseFormat")
    private String purposeOfUseFormat;

    public String getPurposeOfUseRequired() {
        return purposeOfUseRequired;
    }

    public String getSubjectRoleRequired() {
        return subjectRoleRequired;
    }

    public String getPrincipalRequired() {
        return principalRequired;
    }

    public String getPrincipalIdRequired() {
        return principalIdRequired;
    }

    public String getPersonIdRequired() {
        return personIdRequired;
    }

    public String getGroupRequired() {
        return groupRequired;
    }

    public String getGroupIdRequired() {
        return groupIdRequired;
    }

    public String getAccessTokenFormatRequired() {
        return accessTokenFormatRequired;
    }

    public String getAccessTokenFormat() {
        return accessTokenFormat;
    }

    public String getPersonIdFormat() {
        return personIdFormat;
    }

    public String getPurposeOfUseFormat() {
        return purposeOfUseFormat;
    }

    public String getSubjectRoleFormat() {
        return subjectRoleFormat;
    }

    public String getPrincipalIdFormat() {
        return principalIdFormat;
    }

    public String getGroupIdFormat() {
        return groupIdFormat;
    }
}
