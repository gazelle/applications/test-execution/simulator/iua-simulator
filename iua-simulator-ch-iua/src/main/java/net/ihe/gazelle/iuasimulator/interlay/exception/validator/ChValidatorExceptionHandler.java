package net.ihe.gazelle.iuasimulator.interlay.exception.validator;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.interlay.exception.ValidatorErrorMessage;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogAudited;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogLevel;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.ChAuthParamValidationImpl;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.ChScopeParamValidationImpl;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.ChTokenParamValidationImpl;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 12/04/2023
 */
@Provider
@LogAudited(LogLevel.DEBUG)
public class ChValidatorExceptionHandler implements ExceptionMapper<ValidatorException> {

    @Inject
    private ChAuthParamValidationImpl authParam;
    @Inject
    private ChScopeParamValidationImpl scopeParam;
    @Inject
    private ChTokenParamValidationImpl tokenParam;

    @Override
    public Response toResponse(ValidatorException e) {
        if (codeAuthValidation(e)) return getResponse(e);
        if (scopeAuthValidation(e)) return getResponse(e);
        if (tokenPostValidation(e)) return getResponse(e);
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ValidatorErrorMessage("Unexpected error", false)).build();
    }

    private boolean scopeAuthValidation(ValidatorException e) {
        if (e.getMessage().equalsIgnoreCase(scopeParam.getPurposeOfUseRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(scopeParam.getSubjectRoleRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(scopeParam.getPrincipalIdRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(scopeParam.getPrincipalIdRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(scopeParam.getPersonIdRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(scopeParam.getGroupRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(scopeParam.getGroupIdRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(scopeParam.getAccessTokenFormat())) return true;

        if (e.getMessage().equalsIgnoreCase(scopeParam.getPurposeOfUseFormat())) return true;
        if (e.getMessage().equalsIgnoreCase(scopeParam.getPersonIdFormat())) return true;
        return e.getMessage().equalsIgnoreCase(scopeParam.getPersonIdFormat());
    }

    private boolean codeAuthValidation(ValidatorException e) {
        if (e.getMessage().equalsIgnoreCase(authParam.getResponseTypeRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getClientIdRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getBadClientName())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getStateRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getScopeRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getAudienceRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getCodeChallengeRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getRedirectUriRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getCodeChallengeMethodRequired())) return true;

        if (e.getMessage().equalsIgnoreCase(authParam.getUrlFormat())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getBadClientName())) return true;
        return (e.getMessage().equalsIgnoreCase(authParam.getRedirectUriValue()));
    }

    private boolean tokenPostValidation(ValidatorException e) {
        if (e.getMessage().equalsIgnoreCase(tokenParam.getGrantTypeRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(tokenParam.getCodeRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(tokenParam.getCodeVerifierRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(tokenParam.getRedirectUriRequired())) return true;

        if (e.getMessage().equalsIgnoreCase(tokenParam.getUrlFormat())) return true;
        return (e.getMessage().equalsIgnoreCase(tokenParam.getRedirectUriValue()));
    }


    private Response getResponse(ValidatorException e) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ValidatorErrorMessage(e.getMessage(), false)).build();
    }
}
