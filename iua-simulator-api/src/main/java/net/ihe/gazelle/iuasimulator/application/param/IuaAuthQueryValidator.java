package net.ihe.gazelle.iuasimulator.application.param;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 16/04/2023
 */
public interface IuaAuthQueryValidator<T> {
    void validateCodeAuthorization(T iuaCodeParam);
}
