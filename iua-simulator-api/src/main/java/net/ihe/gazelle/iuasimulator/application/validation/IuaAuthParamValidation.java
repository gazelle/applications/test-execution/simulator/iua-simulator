package net.ihe.gazelle.iuasimulator.application.validation;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 17/04/2023
 */
public interface IuaAuthParamValidation {
    String getBadCodeChallengeFormat();

    String getBadCodeChallengeMethod();

    String getBadResponseType();

    String getUrlFormat();

    String getRedirectUriValue();

    String getBadClientName();

    String getResponseTypeRequired();

    String getClientIdRequired();

    String getStateRequired();

    String getCodeChallengeRequired();

    String getRedirectUriRequired();

    String getCodeChallengeMethodRequired();
}
