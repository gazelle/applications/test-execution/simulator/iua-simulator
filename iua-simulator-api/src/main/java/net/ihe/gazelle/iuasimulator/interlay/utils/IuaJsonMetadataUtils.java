package net.ihe.gazelle.iuasimulator.interlay.utils;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;

public class IuaJsonMetadataUtils {

    private static final String SIMULATOR_ENDPOINT = "/iua-simulator/rest/";

    public IuaJsonMetadataUtils(String spec) {
        IuaJsonMetadataUtils.spec = spec;
    }
    private static String spec;

    public static JsonObject generateMetadata(String endpoint){

            JsonObjectBuilder builder = Json.createObjectBuilder();

            builder.addAll(generateIssuer(endpoint));
            builder.addAll(generateAuthorizationEndpoint(endpoint));
            builder.addAll(generateTokenEndpoint(endpoint));
            builder.addAll(generateJWKSUri(endpoint));
            builder.addAll(generateScopesSupported());
            builder.addAll(generateResponseTypesSupported());
            builder.addAll(generateGrantTypesSupported());
            builder.addAll(generateIntrospectionEndpoint(endpoint));
            builder.addAll(generateIntrospectionEndpointAuthMethods());
            builder.addAll(generateAccessTokenFormat());

            return builder.build();
        }

        private static JsonObjectBuilder generateIssuer(String endpoint){
            return Json.createObjectBuilder()
                    .add("issuer", endpoint);
        }

        private static JsonObjectBuilder generateAuthorizationEndpoint(String endpoint){
            return Json.createObjectBuilder()
                    .add("authorization_endpoint", endpoint + SIMULATOR_ENDPOINT + spec + "/authorize");
        }

        private static JsonObjectBuilder generateTokenEndpoint(String endpoint){
            return Json.createObjectBuilder()
                    .add("token_endpoint", endpoint + SIMULATOR_ENDPOINT + spec + "/token");
        }

        private static JsonObjectBuilder generateJWKSUri(String endpoint){
            return Json.createObjectBuilder()
                    .add("jwks_uri", endpoint + SIMULATOR_ENDPOINT + spec + "/certs");
        }


        private static JsonObjectBuilder generateScopesSupported(){
            return Json.createObjectBuilder()
                    .add("scopes", Json.createArrayBuilder()
                            .add("launch")
                            .add("purpose_of_use")
                            .add("subject_role")
                            .add("person_id")
                            .add("principal")
                            .add("principal_id")
                            .add("group")
                            .add("group_id")
                            .add("access_token_format"));
        }


        private static JsonObjectBuilder generateResponseTypesSupported(){
            return Json.createObjectBuilder()
                    .add("response_types_supported", Json.createArrayBuilder()
                            .add("code")
                            .add("token"));
        }

        private static JsonObjectBuilder generateGrantTypesSupported(){
            return Json.createObjectBuilder()
                    .add("grant_types_supported", Json.createArrayBuilder()
                            .add("authorization_code"));
        }

        private static JsonObjectBuilder generateIntrospectionEndpoint(String endpoint){
            return Json.createObjectBuilder()
                    .add("introspection_endpoint", endpoint + SIMULATOR_ENDPOINT + spec + "/token/introspect");
        }
        private static JsonObjectBuilder generateIntrospectionEndpointAuthMethods(){
            return Json.createObjectBuilder()
                    .add("introspection_endpoint", Json.createArrayBuilder()
                            .add("client_secret_basic")
                            .add("client_secret_post")
                            .add("tls_client_auth"));
        }

        private static JsonObjectBuilder generateAccessTokenFormat(){
            return Json.createObjectBuilder()
                    .add("access_token_format", Json.createArrayBuilder()
                            .add("ihe-jwt")
                            .add("ihe-saml"));
        }



}
