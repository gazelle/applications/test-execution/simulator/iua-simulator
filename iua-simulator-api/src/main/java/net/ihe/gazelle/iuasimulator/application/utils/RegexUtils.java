package net.ihe.gazelle.iuasimulator.application.utils;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 17/04/2023
 */
public interface RegexUtils {

    boolean isValidPersonIdFormat(String value);

    boolean isValidPurposeOfUseFormat(String value);

    boolean isValidSubjectRoleFormat(String value);

    boolean isValidGroupIdFormat(String value);

    boolean isValidPrincipalIdFormat(String value);

    boolean isValidAccessTokenFormat(String value);

    boolean isValidResponseTypeValue(String value);

    boolean isValidCodeChallengeMethodValue(String value);

    boolean isValidCodeChallengeFormat(String value);

    boolean isValidURL(String url);
}
