package net.ihe.gazelle.iuasimulator.domain.model;


/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 09/04/2023
 */
public class IuaTokenParamBuilder {

    private final String redirectUri;
    private final String grandType;
    private final String codeVerifier;
    private final String code;

    protected IuaTokenParamBuilder(IuaBuilder<?> iuaBuilder) {
        this.redirectUri = iuaBuilder.redirectUri;
        this.grandType = iuaBuilder.grandType;
        this.codeVerifier = iuaBuilder.codeVerifier;
        this.code = iuaBuilder.code;
    }

    public static IuaBuilder builder() {
        return new IuaBuilder() {
            @Override
            public IuaBuilder getThis() {
                return this;
            }
        };
    }

    public abstract static class IuaBuilder<T extends IuaBuilder<T>> {
        private String redirectUri;
        private String grandType;
        private String codeVerifier;
        private String code;

        public abstract T getThis();

        public T withRedirectUri(String redirectUri) {
            this.redirectUri = redirectUri;
            return this.getThis();
        }
        public T withGrantType(String grandType) {
            this.grandType = grandType;
            return this.getThis();
        }
        public T withCodeVerifier(String codeVerifier) {
            this.codeVerifier = codeVerifier;
            return this.getThis();
        }
        public T withCode(String code) {
            this.code = code;
            return this.getThis();
        }

        public IuaTokenParamBuilder build() {
            return new IuaTokenParamBuilder(this);
        }
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public String getGrandType() {
        return grandType;
    }

    public String getCodeVerifier() {
        return codeVerifier;
    }

    public String getCode() {
        return code;
    }
}
