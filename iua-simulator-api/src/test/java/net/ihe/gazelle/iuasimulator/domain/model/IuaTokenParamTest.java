package net.ihe.gazelle.iuasimulator.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 11/04/2023
 */
class IuaTokenParamTest {

    @Test
    void tokenParamTest() {
        IuaTokenParamBuilder tokenParam = IuaTokenParamBuilder.builder()
                .withGrantType("authorization_code")
                .withRedirectUri("http://localhost:8090/iua-simulator/rest/ch/callback")
                .withCode("17f4f930-9f09-4816-9124-40e7db4398eb.991bbfa9")
                .withCodeVerifier("--5Q635ku8ER2pfGeuDagpsk40~5kdbhPftnIaZubTAHKLXW5ZZQA9")
                .build();

        Assertions.assertEquals("authorization_code", tokenParam.getGrandType());
        Assertions.assertEquals("http://localhost:8090/iua-simulator/rest/ch/callback", tokenParam.getRedirectUri());
        Assertions.assertEquals("17f4f930-9f09-4816-9124-40e7db4398eb.991bbfa9", tokenParam.getCode());
        Assertions.assertEquals("--5Q635ku8ER2pfGeuDagpsk40~5kdbhPftnIaZubTAHKLXW5ZZQA9", tokenParam.getCodeVerifier());
    }

}
