package net.ihe.gazelle.iuasimulator.interlay.resource.validator.param;

import jakarta.inject.Inject;
import net.ihe.gazelle.iuasimulator.application.validation.IuaTokenParamValidation;
import net.ihe.gazelle.iuasimulator.domain.validation.IuaTokenParamValidationImpl;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 13/04/2023
 */
public class IheTokenParamValidationImpl extends IuaTokenParamValidationImpl implements IuaTokenParamValidation {

    @Inject
    @ConfigProperty(name = "iua.code.token.validator.exception.grantType")
    private String grantTypeRequired;
    @Inject
    @ConfigProperty(name = "iua.code.token.validator.exception.code")
    private String codeRequired;
    @Inject
    @ConfigProperty(name = "iua.code.token.validator.exception.codeVerifier")
    private String codeVerifierRequired;
    @Inject
    @ConfigProperty(name = "iua.code.token.validator.exception.redirectUri")
    private String redirectUriRequired;
    @Inject
    @ConfigProperty(name = "iua.code.token.validator.exception.urlFormat")
    private String urlFormat;
    @Inject
    @ConfigProperty(name = "iua.code.token.validator.exception.redirectUriValue")
    private String redirectUriValue;

    @Override
    public String getGrantTypeRequired() {
        return grantTypeRequired;
    }

    @Override
    public String getCodeRequired() {
        return codeRequired;
    }

    @Override
    public String getCodeVerifierRequired() {
        return codeVerifierRequired;
    }

    @Override
    public String getRedirectUriRequired() {
        return redirectUriRequired;
    }

    @Override
    public String getUrlFormat() {
        return urlFormat;
    }

    @Override
    public String getRedirectUriValue() {
        return redirectUriValue;
    }
}
