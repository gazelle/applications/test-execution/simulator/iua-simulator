package net.ihe.gazelle.iuasimulator.interlay.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 06/02/2023
 */
public class LogService {
    // Receives the class name decorated with @LogAudited
    public void log(final String clazz, final LogLevel level, final String message) {
        // Logger from package java.util.logging
        Logger log = LoggerFactory.getLogger(clazz);
        switch (level) {
            case INFO:
                log.info(message);
                break;
            case DEBUG:
                log.debug(message);
                break;
            case WARNING:
                log.warn(message);
                break;
            case SEVERE:
                log.error(message);
                break;
            default:
                log.trace(message);
                break;
        }
    }
}
