package net.ihe.gazelle.iuasimulator.interlay.configuration;

import net.ihe.gazelle.iuasimulator.interlay.configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 13/02/2023
 */
class ConfigurationTest {

    Configuration configuration = Configuration.getinstance();

    @Test
    void getConfigurationTest() {
        Assertions.assertNotNull(configuration.getValue("version"));
    }
}
