package net.ihe.gazelle.iuasimulator.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 14/04/2023
 */
class IheCodeParamTest {

    @Test
    void getIuaCodeParamTest() {
        IheAuthParamBuilder iuaCodeParam = IheAuthParamBuilder.builder()
                .withResponseType("responseType")
                .withClientId("clientId")
                .withRedirectUri("redirectUri")
                .withResource("resource")
                .withState("state")
                .withScope("scope")
                .withAudience("audience")
                .withCodeChallenge("codeChallenge")
                .withCodeChallengeMethod("S256").build();
        Assertions.assertEquals("responseType", iuaCodeParam.getResponseType());
        Assertions.assertEquals("clientId", iuaCodeParam.getClientId());
        Assertions.assertEquals("redirectUri", iuaCodeParam.getRedirectUri());
        Assertions.assertEquals("resource", iuaCodeParam.getResource());
        Assertions.assertEquals("state", iuaCodeParam.getState());
        Assertions.assertEquals("scope", iuaCodeParam.getScope());
        Assertions.assertEquals("audience", iuaCodeParam.getAudience());
        Assertions.assertEquals("codeChallenge", iuaCodeParam.getCodeChallenge());
        Assertions.assertEquals("S256", iuaCodeParam.getCodeChallengeMethod());
    }
}