#!/bin/bash

# Enable debug mode
if [ "${DEBUG_MODE_ENABLED}" = true ]; then
  JAVA_EXTRA_ARGS="--debug *:${DEBUG_MODE_PORT:-"8787"}"
fi

if [ "${DEBUG_REMOTE_ENABLED:-false}" = "true" ]; then
  export JAVA_OPTS="$JAVA_OPTS -Xrunjdwp:transport=dt_socket,address=${JBOSS_HTTP_REMOTE_DEBUG_PORT},server=y,suspend=n"
fi

if [ "${JBOSS_PORTS_OFFSET_ENABLED:-false}" = "true" ]
  then
    export JAVA_OPTS="$JAVA_OPTS -Djboss.socket.binding.port-offset=${JBOSS_PORTS_OFFSET} -Djboss.http.port=8080"
fi


${JBOSS_HOME}/bin/standalone.sh -b ${JBOSS_BIND:-0.0.0.0}
